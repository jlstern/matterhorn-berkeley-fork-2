/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.handler;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;

import org.opencastproject.composer.api.ComposerService;
import org.opencastproject.composer.api.EncoderException;
import org.opencastproject.composer.api.EncodingProfileImpl;
import org.opencastproject.job.api.Job;
import org.opencastproject.job.api.JobContext;
import org.opencastproject.mediapackage.MediaPackage;
import org.opencastproject.mediapackage.MediaPackageElementFlavor;
import org.opencastproject.mediapackage.MediaPackageException;
import org.opencastproject.mediapackage.Publication;
import org.opencastproject.mediapackage.Track;
import org.opencastproject.mediapackage.VideoStream;
import org.opencastproject.mediapackage.identifier.IdImpl;
import org.opencastproject.mediapackage.track.TrackImpl;
import org.opencastproject.mediapackage.track.VideoStreamImpl;
import org.opencastproject.publication.api.PublicationException;
import org.opencastproject.serviceregistry.api.ServiceRegistry;
import org.opencastproject.serviceregistry.api.ServiceRegistryException;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.workflow.api.WorkflowInstance;
import org.opencastproject.workflow.api.WorkflowOperationException;
import org.opencastproject.workflow.api.WorkflowOperationInstance;
import org.opencastproject.workflow.api.WorkflowOperationResult;

import de.schlichtherle.io.File;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.LinkedList;

/**
 * @author John Crossman
 */
@Ignore
public class MergeVideosWorkflowOperationHandlerTest {

  private final MergeVideosWorkflowOperationHandler handler = new MergeVideosWorkflowOperationHandler();
  private final ComposerService composerService = createMock(ComposerService.class);
  private final ServiceRegistry serviceRegistry = createMock(ServiceRegistry.class);
  private final WorkflowOperationInstance operationInstance = createMock(WorkflowOperationInstance.class);

  private final String encodingProfileId = "video-overlay";

  @Before
  public void before() {
    handler.setComposerService(composerService);
    handler.setServiceRegistry(serviceRegistry);
    //
    expect(operationInstance.getConfiguration("source-tags")).andReturn("youtubein").once();
    expect(operationInstance.getConfiguration("source-flavors")).andReturn("presenter/trimmed,presentation/trimmed").once();
    expect(operationInstance.getConfiguration("target-flavor")).andReturn("youtube/publishable").once();
    expect(operationInstance.getConfiguration("target-tags")).andReturn("youtubein").once();
    expect(operationInstance.getConfiguration("encoding-profile")).andReturn(encodingProfileId).once();
  }

  @Test
  public void testLogicUsingMockObjects() throws WorkflowOperationException, PublicationException, NotFoundException, ServiceRegistryException, IOException, URISyntaxException, MediaPackageException, EncoderException {
    final String trackXML = "<track id=\"track-1\" type=\"presentation/source\"><mimetype>video/quicktime</mimetype>"
        + "<url>http://localhost:8080/workflow/samples/camera.mpg</url>"
        + "<checksum type=\"md5\">43b7d843b02c4a429b2f547a4f230d31</checksum><duration>14546</duration>"
        + "<video><device type=\"UFG03\" version=\"30112007\" vendor=\"Unigraf\" />"
        + "<encoder type=\"H.264\" version=\"7.4\" vendor=\"Apple Inc\" /><resolution>640x480</resolution>"
        + "<scanType type=\"progressive\" /><bitrate>540520</bitrate><frameRate>2</frameRate></video></track>";
    final String publicationXML = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
        + " <oc:publication id=\"p-1\" channel=\"engage\">\n"
        + "   <oc:mimetype>text/html</oc:mimetype>\n"
        + "   <oc:url>http://localhost/engage.html</oc:url>\n"
        + " </oc:publication>";
    //
    final WorkflowInstance workflowInstance = createMock(WorkflowInstance.class);
    final JobContext jobContext = createMock(JobContext.class);
    final MediaPackage mediaPackage = createMock(MediaPackage.class);
    final Job overlayJob = createMock(Job.class);
    final long jobId = 1;
    final long publishJobId = 2;
    //
    expect(workflowInstance.getCurrentOperation()).andReturn(operationInstance).once();
    expect(workflowInstance.getMediaPackage()).andReturn(mediaPackage).once();
    //
    final Track presenterTrack = createTrack("presenter", "trimmed");
    final Track presentationTrack = createTrack("presentation", "trimmed");
    //
    expect(mediaPackage.getTrack("presenter/trimmed")).andReturn(presenterTrack).once();
    expect(mediaPackage.getTrack("presentation/trimmed")).andReturn(presentationTrack).once();
    expect(mediaPackage.getIdentifier()).andReturn(new IdImpl()).once();
    mediaPackage.add(anyObject(Track.class));
    expectLastCall().once();
    //
    final EncodingProfileImpl encodingProfile = new EncodingProfileImpl();
    encodingProfile.setIdentifier(encodingProfileId);
    expect(composerService.getProfile(encodingProfileId)).andReturn(encodingProfile).once();
    expect(composerService.mergeVideoFiles(presenterTrack, presentationTrack, encodingProfile.getIdentifier())).andReturn(overlayJob).once();
    //
    expect(overlayJob.getId()).andReturn(jobId).once();
    expect(overlayJob.getStatus()).andReturn(Job.Status.FINISHED).once();
    overlayJob.setStatus(Job.Status.FINISHED);
    expectLastCall().once();
    expect(overlayJob.getPayload()).andReturn(trackXML).times(2);
    overlayJob.setPayload(trackXML);
    expectLastCall().once();
    final long queueTime = 200;
    expect(overlayJob.getQueueTime()).andReturn(queueTime).once();
    //
    expect(serviceRegistry.getJob(jobId)).andReturn(overlayJob).once();
    //
    final Job publishJob = createMock(Job.class);
    expect(publishJob.getId()).andReturn(publishJobId).times(2);
    expect(publishJob.getStatus()).andReturn(Job.Status.FINISHED).once();
    publishJob.setStatus(Job.Status.FINISHED);
    expectLastCall().once();
    expect(serviceRegistry.getJob(publishJobId)).andReturn(publishJob).times(2);
    expect(publishJob.getPayload()).andReturn(publicationXML).times(3);
    publishJob.setPayload(publicationXML);
    expectLastCall().once();
    mediaPackage.add(anyObject(Publication.class));
    expectLastCall().once();
    //
    replay(serviceRegistry, composerService, workflowInstance, operationInstance,
        mediaPackage, jobContext, overlayJob, publishJob);
    //
    final WorkflowOperationResult result = handler.start(workflowInstance, jobContext);
    assertNotNull(result);
    assertEquals(WorkflowOperationResult.Action.CONTINUE, result.getAction());
  }

  private Track createTrack(final String type, final String subType) throws IOException {
    final TrackImpl track = new TrackImpl();
    track.setURI(File.createTempFile(type, subType).toURI());
    track.setFlavor(new MediaPackageElementFlavor(type, subType));
    final LinkedList<VideoStream> video = new LinkedList<VideoStream>();
    video.add(new VideoStreamImpl());
    track.setVideo(video);
    return track;
  }

}
