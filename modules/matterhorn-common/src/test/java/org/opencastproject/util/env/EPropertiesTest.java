/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class EPropertiesTest {

  private final Dictionary<String, String> propertyOverrides = EnvironmentUtil.getRuntimePropertyOverrides();

  @Test
  public void testStandardProperties() {
    final Map<String, String> map = new HashMap<String, String>();
    // This next property key has "_" but "two" will not match a value of Environment enum so it should be treated as a standard property.
    map.put("one_plus_two", "3");
    map.put("_four", "4");
    map.put("five", "5");
    // This next property key has "_prod" but it doesn't end with "_prod" so it should be treated as a standard property.
    map.put("five_prodr", "5");
    final Properties properties = new Properties();
    for (final String key : map.keySet()) {
      properties.setProperty(key, map.get(key));
    }
    final Dictionary d = new EProperties(properties);
    final int expectedSize = map.size() + EnvironmentUtil.getRuntimePropertyOverrides().size();
    assertEquals(expectedSize, d.size());
    for (final String key : map.keySet()) {
      assertEquals(map.get(key), d.get(key));
    }
  }

  @Test
  public void testApplyHostPropertyOverrides() throws IOException {
    final List<String> lines = new LinkedList<String>();
    lines.add("hello=World");
    // We expect an override of these next properties
    lines.add(PropertyOverride.matterhornProfile.getKey() + "=foo");
    lines.add(PropertyOverride.matterhornEnvironment.getKey() + "=prod");
    lines.add(PropertyOverride.matterhornDomain.getKey() + "=we will override this property");
    //
    final File file = File.createTempFile("test-config", ".properties");
    FileUtils.writeLines(file, lines, "\n");
    final Properties properties = new Properties();
    properties.load(new FileInputStream(file));
    //
    final Dictionary result = new EProperties(properties);
    // The "hello=World" is only property that was not overridden so:
    assertEquals(propertyOverrides.size() + 1, result.size());
    assertEquals("World", result.get("hello"));
    for (final PropertyOverride propertyOverride : PropertyOverride.values()) {
      final String key = propertyOverride.getKey();
      final Object resultValue = result.get(key);
      assertTrue("Unexpected type: " + resultValue.getClass().getName(), resultValue instanceof String);
      assertTrue("Unexpected: " + resultValue, propertyOverrides.get(key).equals(resultValue));
    }
  }

}
