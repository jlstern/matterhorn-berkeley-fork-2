/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author John Crossman
 */
public class EnvironmentUtilTest {

  private static final File matterhornHome = FileUtils.getTempDirectory();
  private static File informationDir;
  private static boolean preserveInformationDir;
  private static File informationDirBackup;

  @BeforeClass
  public static void beforeClass() throws IOException {
    System.setProperty(EnvironmentUtil.matterhornHomeKey, matterhornHome.getAbsolutePath());
    informationDir = new File(matterhornHome, "lib/build-information");
    preserveInformationDir = informationDir.exists();
    informationDirBackup = new File(matterhornHome, "lib/build-information-BACKUP");
    //
    assertTrue(matterhornHome.exists());
    if (informationDir.exists()) {
      FileUtils.moveDirectory(informationDir, informationDirBackup);
    }
    if (!informationDir.exists()) {
      assertTrue(informationDir.mkdirs());
    }
    assertTrue(informationDir.exists());
  }

  @AfterClass
  public static void afterClass() throws IOException {
    if (preserveInformationDir) {
      FileUtils.moveDirectory(informationDirBackup, informationDir);
    } else {
      FileUtils.deleteDirectory(informationDir);
    }
    FileUtils.deleteDirectory(matterhornHome);
  }

  @Test
  public void testGetMatterhornHome() {
    assertNotNull(EnvironmentUtil.getMatterhornHome());
  }

  @Test
  public void testApplicationProfileMatcher() {
    for (final Environment environment : Environment.values()) {
      testApplicationProfileMatcher(environment, ApplicationProfile.engage, "playback", false);
      testApplicationProfileMatcher(environment, ApplicationProfile.courses, "video", false);
    }
  }

  private void testApplicationProfileMatcher(final Environment environment, final ApplicationProfile profile, final String subDomain, final boolean isHttps) {
    final ApplicationProfile expectedProfile = environment.equals(Environment.local) ? ApplicationProfile.courses : profile;
    final String serverHostName;
    final String expectedURL;
    final String protocol = isHttps ? "https://" : "http://";
    switch (environment) {
      case local:
        serverHostName = "localhost";
        expectedURL = protocol + serverHostName + ":8080";
        break;
      case dev:
      case ci:
      case qa:
        serverHostName = subDomain + "-" + environment.name() + ".ets.berkeley.edu";
        expectedURL = protocol + serverHostName;
        break;
      case staging:
      case prod:
        serverHostName = subDomain + ".berkeley.edu";
        expectedURL = protocol + serverHostName;
        break;
      default:
        throw new UnsupportedOperationException("This unit test is not supported in environment = " + environment);
    }
    final String failedWhen = "environment: " + environment + "; serverHostName: " + serverHostName + "; profile: " + profile;
    //
    final ApplicationProfile actualProfile = new ApplicationProfileMatcher(true, environment).findMatch(serverHostName);
    assertEquals(failedWhen, expectedProfile, actualProfile);
    final String actualURL = EnvironmentUtil.getBaseURL(expectedProfile, environment);
    // This is a hack but I don't want to create a mock call to runtime config file so I can override 'matterhorn.domain' value
    final String cleansedURL = environment.equals(Environment.prod) || environment.equals(Environment.staging) ? actualURL.replace(".ets", "") : actualURL;
    assertEquals(failedWhen, expectedURL, cleansedURL);
  }

  @Test
  public void testEProperties() {
    final Map<String, String> map = new HashMap<String, String>();
    map.put("key1", "1_default");
    map.put("key1_local", "1_local");
    map.put("key1_ci", "1_ci");
    map.put("key1_dev", "1_dev");
    map.put("key1_qa", "1_qa");
    map.put("key1_prod", "1_prod");
    map.put("key2", "2");

    map.put("key3", "3_default");
    for (final Environment e : Environment.values()) {
      if (e != EnvironmentUtil.getEnvironment()) {
        map.put("key3_" + e.name(), "3_" + e.name());
      }
    }
    final Properties properties = new Properties();
    for (final String key : map.keySet()) {
      properties.setProperty(key, map.get(key));
    }
    final Environment environment = EnvironmentUtil.getEnvironment();
    final Dictionary d = new EProperties(properties);
    assertEquals("Environment: " + environment, d.get("key1"), "1_" + environment.name());
    assertEquals("Environment: " + environment, d.get("key2"), "2");
    assertEquals("Environment: " + environment, d.get("key3"), "3_default");
  }

  @Ignore("EnvironmentUtil needs to pull from the temp dir for this to work")
  public void testGetRevisionNumber() throws IOException {
    final File revisionNumberFile = new File(informationDir, "revisionNumber");
    final File buildNumberFile = new File(informationDir, "buildNumber");
    FileUtils.writeStringToFile(revisionNumberFile, "\n  123\n  ");
    // TODO: Why does CheckStyle fail when string has an embedded line break?
    FileUtils.writeStringToFile(buildNumberFile, "  456  " + '\n' + "  ");
    //
    System.out.println(revisionNumberFile.getAbsolutePath() + " contains " + FileUtils.readFileToString(revisionNumberFile));
    System.out.println(buildNumberFile.getAbsolutePath() + " contains " + FileUtils.readFileToString(buildNumberFile));
    assertEquals("123", EnvironmentUtil.getGitRevision());
    assertEquals("456", EnvironmentUtil.getBambooBuild());
  }

  @Test
  public void testGetHostConfigurationsHome() {
    final File directory = EnvironmentUtil.getHostConfigurationsHome();
    assertTrue(directory.exists());
    assertTrue(new File(directory, ".mhruntime.cf").exists());
  }

}
