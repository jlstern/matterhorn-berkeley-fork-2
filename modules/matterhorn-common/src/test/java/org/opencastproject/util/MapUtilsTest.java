/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

/**
 * @author John Crossman
 */
public class MapUtilsTest {

  private Map<String, String> sampleMap;

  @Before
  public void before() {
    sampleMap = new HashMap<String, String>();
    sampleMap.put("one", "1");
    sampleMap.put("two", "2");
    sampleMap.put("three", "3");
    sampleMap.put("four", "4");
  }

  @Test
  public void testMergeZero() {
    assertEquals(4, MapUtils.merge(sampleMap, new Properties()).size());
  }

  @Test
  public void testMerge() {
    final Properties properties = new Properties();
    properties.put("five", "5");
    properties.put("six", "6");
    final Map<String, String> merge = MapUtils.merge(sampleMap, properties);
    assertEquals(6, merge.size());
    assertEquals("5", merge.get("five"));
    assertEquals("6", merge.get("six"));
  }

  @Test(expected = IllegalArgumentException.class)
  public void testMergeException() {
    final Properties properties = new Properties();
    properties.put(4, "four");
    MapUtils.merge(sampleMap, properties);
  }

  @Test
  public void testToPropertiesEmpty() {
    final Properties properties = MapUtils.toProperties(new HashMap<String, String>());
    assertEquals(0, properties.size());
  }

  @Test
  public void testToProperties() {
    final HashMap<String, String> map = new HashMap<String, String>();
    map.put("10", "1");
    map.put("20", "2");
    map.put("30", "3");
    final Properties properties = MapUtils.toProperties(map);
    assertEquals(3, properties.size());
    //
    assertEquals("1", properties.get("10"));
    assertEquals("2", properties.get("20"));
    assertEquals("3", properties.get("30"));
  }

  @Test
  public void testClone() {
    final Dictionary<String, Integer> d1 = new Hashtable<String, Integer>();
    d1.put("10", 1);
    d1.put("20", 2);
    d1.put("30", 3);
    final Dictionary<String, Integer> d2 = MapUtils.clone(d1);
    assertNotSame(d1, d2);
    assertEquals(d1.size(), d2.size());
    //
    assertTrue(1 == d2.get("10"));
    assertTrue(2 == d2.get("20"));
    assertTrue(3 == d2.get("30"));
  }

}
