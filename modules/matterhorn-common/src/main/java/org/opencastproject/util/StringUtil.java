/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.exception.ExceptionUtils;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;

/**
 * @author John Crossman
 */
public final class StringUtil {

  private static final String NULL_AS_STRING = "[null]";

  private StringUtil() {
  }

  /**
   * @param map never null
   * @return list properties in single string
   */
  public static String toString(final Map map) {
    if (map == null) {
      return NULL_AS_STRING;
    }
    final StringBuilder b = new StringBuilder();
    for (final Object key : map.keySet()) {
      appendMapEntry(b, key, map.get(key));
    }
    return b.toString();
  }

  /**
   * @param properties never null
   * @return list properties in single string
   */
  public static String toString(final Properties properties) {
    if (properties == null) {
      return NULL_AS_STRING;
    }
    final StringBuilder b = new StringBuilder();
    for (final Object key : properties.keySet()) {
      appendMapEntry(b, key, properties.get(key));
    }
    return b.toString();
  }

  /**
   * @param properties never null
   * @return list properties in single string
   */
  public static String toString(final Dictionary properties) {
    if (properties == null) {
      return NULL_AS_STRING;
    }
    final StringBuilder b = new StringBuilder();
    final Enumeration e = properties.elements();
    while (e.hasMoreElements()) {
      final Object key = e.nextElement();
      appendMapEntry(b, key, properties.get(key));
    }
    return b.toString();
  }

  /**
   * @see #reflectionToString(Object, org.apache.commons.lang.builder.ToStringStyle)
   * @param content null allowed
   * @return we do our best to give back something meaningful
   */
  public static String toStringObject(final Object content) {
    return reflectionToString(content, ToStringStyle.SIMPLE_STYLE);
  }

  /**
   * @see #reflectionToString(Object, org.apache.commons.lang.builder.ToStringStyle)
   * @param content null allowed
   * @return we do our best to give back something meaningful
   */
  public static String reflectionToStringMultiLine(final Object content) {
    return reflectionToString(content, ToStringStyle.MULTI_LINE_STYLE);
  }

  /**
   * @param e never null
   * @return the message and full stack trace
   */
  public static String toStringException(final Throwable e) {
    return ExceptionUtils.getMessage(e) + '\n' + ExceptionUtils.getFullStackTrace(e);
  }

  /**
   * Use this when you do not have faith in the {@link Object#toString()} impl of the object in question.
   * @see ToStringBuilder
   * @param content null allowed
   * @param toStringStyle single line or multi-line or?
   * @return never null
   */
  private static String reflectionToString(final Object content, final ToStringStyle toStringStyle) {
    final String s;
    if (content == null) {
      s = NULL_AS_STRING;
    } else {
      try {
        s = (content instanceof String)
            ? (String) content
            : content.getClass().getName().concat(":").concat(ToStringBuilder.reflectionToString(content, toStringStyle));
      } catch (final Throwable e) {
        return "Exception caught during toString on ".concat(content.getClass().getSimpleName()).concat(". Exception:\n")
            .concat(ExceptionUtils.getMessage(e))
            .concat("\n")
            .concat(ExceptionUtils.getFullStackTrace(e));
      }
    }
    return s;
  }

  private static void appendMapEntry(final StringBuilder b, final Object key, final Object value) {
    if (b.length() > 0) {
      b.append("; ");
    }
    final boolean isPassword = (key instanceof String) && StringUtils.containsIgnoreCase((String) key, "password");
    final String valueAsString = isPassword ? "[toString() will not show password info]" : toStringObject(value);
    b.append(toStringObject(key)).append("=").append(valueAsString);
  }

  public static String asTwoDigitString(final int number) {
    return number < 10 ? "0" + number : "" + number;
  }

  public static String replaceAll(String template, final Map<? extends Enum, String> queryArgMap) {
    for (final Enum key : queryArgMap.keySet()) {
      template = template.replaceAll("\\{" + key.name() + "\\}", queryArgMap.get(key));
    }
    return template;
  }

}
