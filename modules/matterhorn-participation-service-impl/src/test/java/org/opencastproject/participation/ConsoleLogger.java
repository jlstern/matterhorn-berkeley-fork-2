/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation;

import org.apache.commons.lang.ClassUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.ops4j.pax.logging.PaxContext;
import org.ops4j.pax.logging.PaxLogger;
import org.ops4j.pax.logging.slf4j.Slf4jLogger;

/**
 * @author John Crossman
 */
public class ConsoleLogger extends Slf4jLogger {

  public ConsoleLogger(final Class clazz) {
    super(ClassUtils.getShortCanonicalName(clazz), new ConsolePaxLogger(clazz));
  }

  private static class ConsolePaxLogger implements PaxLogger {

    private final Class clazz;

    public ConsolePaxLogger(final Class clazz) {
      this.clazz = clazz;
    }

    @Override
    public boolean isTraceEnabled() {
      return true;
    }

    @Override
    public boolean isDebugEnabled() {
      return true;
    }

    @Override
    public boolean isWarnEnabled() {
      return true;
    }

    @Override
    public boolean isInfoEnabled() {
      return true;
    }

    @Override
    public boolean isErrorEnabled() {
      return true;
    }

    @Override
    public boolean isFatalEnabled() {
      return true;
    }

    @Override
    public void trace(final String message, final Throwable throwable) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
    }

    @Override
    public void debug(final String message, final Throwable throwable) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
    }

    @Override
    public void inform(final String message, final Throwable throwable) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
    }

    @Override
    public void warn(final String message, final Throwable throwable) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
    }

    @Override
    public void error(final String message, final Throwable throwable) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
    }

    @Override
    public void fatal(final String message, final Throwable throwable) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
    }

    @Override
    public void trace(final String message, final Throwable throwable, final String s2) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
      System.out.println(s2);
    }

    @Override
    public void debug(final String message, final Throwable throwable, final String s2) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
      System.out.println(s2);
    }

    @Override
    public void inform(final String message, final Throwable throwable, final String s2) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
      System.out.println(s2);
    }

    @Override
    public void warn(final String message, final Throwable throwable, final String s2) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
      System.out.println(s2);
    }

    @Override
    public void error(final String message, final Throwable throwable, final String s2) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
      System.out.println(s2);
    }

    @Override
    public void fatal(final String message, final Throwable throwable, final String s2) {
      System.out.println(message);
      System.out.println(ExceptionUtils.getFullStackTrace(throwable));
      System.out.println(s2);
    }

    @Override
    public int getLogLevel() {
      return LEVEL_TRACE;
    }

    @Override
    public String getName() {
      return ClassUtils.getShortCanonicalName(clazz);
    }

    @Override
    public PaxContext getPaxContext() {
      return new PaxContext();
    }
  }
}
