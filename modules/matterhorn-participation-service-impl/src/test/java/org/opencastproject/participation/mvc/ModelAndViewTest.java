/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.junit.Test;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.security.api.User;
import org.opencastproject.util.data.Collections;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;

/**
 * @author John Crossman
 */
public class ModelAndViewTest {

  private final ModelAndView mav;

  public ModelAndViewTest() throws IOException {
    mav = new ModelAndView("signUp");
  }

  @Test
  public void testPut() {
    final String duck = "Duck!";
    mav.put(Key.error, duck);
    assertEquals(duck, mav.get(Key.error));
    assertEquals(duck, mav.getMap().get("error"));
  }

  @Test
  public void testPutByConvention() {
    final User user = new User();
    mav.put(user);
    assertSame(user, mav.getMap().get("user"));
  }

  @Test
  public void testPutArrayByConvention() {
    final RecordingType[] values = RecordingType.values();
    mav.put(values);
    assertSame(values, mav.getMap().get("recordingTypeList"));
  }

  @Test
  public void testPutListByConvention() {
    mav.put(Collections.list(RecordingType.values()));
    assertNotNull(mav.getMap().get("recordingTypeList"));
  }

}
