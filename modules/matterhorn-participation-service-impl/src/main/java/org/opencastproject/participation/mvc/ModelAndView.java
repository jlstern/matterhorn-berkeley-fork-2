/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.mvc;

import org.apache.commons.lang.StringUtils;
import org.springframework.core.Conventions;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author John Crossman
 */
public class ModelAndView {

  private final Map<String, Object> map;
  private final String viewName;

  public ModelAndView(final String viewName) throws IOException {
    map = new HashMap<String, Object>();
    this.viewName = viewName;
  }

  public void put(final Object obj) {
    if (obj != null) {
      put(Conventions.getVariableName(obj), obj);
    }
  }

  public void put(final Key key, final Object obj) {
    put(key.name(), obj);
  }

  private void put(final String key, final Object obj) {
    if (obj != null) {
      map.put(key, obj);
    }
  }

  public Map<String, Object> getMap() {
    return map;
  }

  public Object get(final Key key) {
    return map.get(key.name());
  }

  public String getViewName() {
    return viewName;
  }
}
