/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.salesforce.HasSalesforceId;

import org.apache.commons.lang.builder.ToStringBuilder;

public final class Instructor implements HasSalesforceId {

  private String salesforceID;
  private String calNetUID;
  private String role;
  private String firstName;
  private String lastName;
  private String email;
  private String department;

  public Instructor() {
  }

  public Instructor(final String calNetUID) {
    this.calNetUID = calNetUID;
  }

  public String getSalesforceID() {
    return salesforceID;
  }

  public void setSalesforceID(String salesforceID) {
    this.salesforceID = salesforceID;
  }

  public String getCalNetUID() {
    return calNetUID;
  }

  public void setCalNetUID(final String calNetUID) {
    this.calNetUID = calNetUID;
  }

  public String getRole() {
    return role;
  }

  public void setRole(final String role) {
    this.role = role;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(final String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(final String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(final String email) {
    this.email = email;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }

  @Override
  public boolean equals(final Object o) {
    // Matching calNetUID is all it takes
    return (o instanceof Instructor) && ((Instructor) o).getCalNetUID().equals(calNetUID);
  }

  @Override
  public int hashCode() {
    return calNetUID.hashCode();
  }

}
