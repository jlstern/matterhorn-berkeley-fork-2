/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.opencastproject.participation.model.Booking;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.util.StringUtil;
import org.opencastproject.util.env.EnvironmentUtil;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author John Crossman
 */
public class UCBerkeleyCourseDatabaseImpl implements ProvidesCourseCatalogData {

  private enum QueryType {
    getCourseInstructors, getCourseOfferingsOfSemester,;
  }

  private enum QueryArgKey {
    termCd, termYr, courseControlNumber,;
  }

  private final Map<QueryType, String> queryMap;

  private final DatabaseCredentials dbCredentials;

  /**
   * @see oracle.jdbc.OracleDriver
   * @param dbCredentials for database connection
   * @throws IOException when required resource files not found
   */
  public UCBerkeleyCourseDatabaseImpl(final DatabaseCredentials dbCredentials) throws IOException {
    this.dbCredentials = dbCredentials;
    queryMap = getSQLStatementMap();
  }

  @Override
  public Set<CourseData> getCourseData(final Semester semester, final int year) {
    final Set<CourseData> courseDataListSet = new HashSet<CourseData>();
    //
    final Map<QueryArgKey, String> courseQueryArgs = new HashMap<QueryArgKey, String>();
    courseQueryArgs.put(QueryArgKey.termCd, semester.getTermCode());
    courseQueryArgs.put(QueryArgKey.termYr, Integer.toString(year));
    //
    final ResultSetRowConverter<CourseData> converter = new ResultConverterForCourse();
    final Set<CourseData> courseDataSet = getSet(converter, QueryType.getCourseOfferingsOfSemester, courseQueryArgs);
    for (final CourseData courseData : courseDataSet) {
      final boolean validRoom = StringUtils.isNotBlank(courseData.getRoom().getBuilding());
      final boolean validStartEndTimes = courseData.getStartTime() != null && courseData.getEndTime() != null;
      final boolean validMeetingDays = courseData.getMeetingDays() != null && courseData.getMeetingDays().size() > 0;
      if (validRoom && validStartEndTimes && validMeetingDays) {
        courseDataListSet.add(courseData);
      }
    }
    for (final CourseData courseData : courseDataListSet) {
      final Map<QueryArgKey, String> queryArgMap = new HashMap<QueryArgKey, String>();
      queryArgMap.put(QueryArgKey.courseControlNumber, Integer.toString(courseData.getCanonicalCourse().getCcn()));
      queryArgMap.put(QueryArgKey.termCd, semester.getTermCode());
      queryArgMap.put(QueryArgKey.termYr, Integer.toString(year));
      final Set<Instructor> instructorSet = getSet(new ResultConverterForInstructor(), QueryType.getCourseInstructors, queryArgMap);
      courseData.setParticipationSet(toParticipationSet(instructorSet));
    }
    // Reconcile cross-listed courses before returning
    return Booking.reconcileCrossListedCourses(courseDataListSet);
  }

  private Set<Participation> toParticipationSet(final Set<Instructor> instructorSet) {
    final Set<Participation> participationSet = new HashSet<Participation>();
    for (final Instructor instructor : instructorSet) {
      final Participation wrapper = new Participation();
      wrapper.setInstructor(instructor);
      participationSet.add(wrapper);
    }
    return participationSet;
  }

  private <T> Set<T> getSet(final ResultSetRowConverter<T> rowConverter, final QueryType queryType, final Map<QueryArgKey, String> queryArgMap) throws CourseDatabaseException {
    try {
      final Connection connection = getConnection();
      final Statement statement = connection.createStatement();
      final String query = StringUtil.replaceAll(queryMap.get(queryType), queryArgMap);
      final ResultSet resultSet = statement.executeQuery(query);
      final Set<T> set = new HashSet<T>();
      while (resultSet.next()) {
        set.add(rowConverter.convert(resultSet));
      }
      closeWithTolerance(statement);
      closeWithTolerance(resultSet);
      connection.close();
      return set;
    } catch (final SQLException e) {
      throw new CourseDatabaseException("Failed to connect and/or run query: " + queryType, e);
    }
  }

  private Connection getConnection() throws SQLException {
    final String jdbcDriver = "oracle.jdbc.driver.OracleDriver";
    final Date beforeAttempt = new Date();
    try {
      Class.forName(jdbcDriver);
      final int fiveMinutes = 300;
      DriverManager.setLoginTimeout(fiveMinutes);
      return DriverManager.getConnection(dbCredentials.getURL(), dbCredentials.getUsername(), dbCredentials.getPassword());
    } catch (final SQLException e) {
      final long seconds = (new Date().getTime() - beforeAttempt.getTime()) / 1000;
      final String message = seconds + " seconds after invoking DriverManager.getConnection(): " + e.getMessage();
      throw new CourseDatabaseException(message, e);
    } catch (final ClassNotFoundException e) {
      throw new SQLException("Unable to load JDBC driver: " + jdbcDriver, e);
    }
  }

  private void closeWithTolerance(final Object obj) {
    try {
      if (obj instanceof ResultSet) {
        ((ResultSet) obj).close();
      } else if (obj instanceof Statement) {
        ((Statement) obj).close();
      }
    } catch (final SQLException ignore) {
      // We swallow this exception so the connection.close() is still invoked.
      ignore.printStackTrace();
    }
  }

  private Map<QueryType, String> getSQLStatementMap() throws IOException {
    final String relativePath = FilenameUtils.concat("etc/services", UCBerkeleyCourseDatabaseImpl.class.getName() + ".xml");
    final File file = EnvironmentUtil.getFileUnderFelixHome(relativePath);
    final Properties properties = new Properties();
    properties.loadFromXML(new FileSystemResource(file).getInputStream());
    final Map<QueryType, String> map = new HashMap<QueryType, String>();
    for (final Object obj : properties.keySet()) {
      final String keyString = (String) obj;
      final QueryType key = QueryType.valueOf(keyString);
      map.put(key, properties.getProperty(keyString));
    }
    return map;
  }

}
