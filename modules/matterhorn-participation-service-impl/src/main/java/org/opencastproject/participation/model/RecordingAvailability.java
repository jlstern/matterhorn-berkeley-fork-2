/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.model;

import org.opencastproject.salesforce.RepresentsSalesforceField;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlEnum;

/**
 * @author Fernando Alvarez
 */
@XmlEnum
public enum RecordingAvailability implements RepresentsSalesforceField {

  publicCreativeCommons("Public, licensed as Creative Commons 3.0-BY-NC-ND", License.creativeCommons, PublicationChannel.Matterhorn, PublicationChannel.YouTube, PublicationChannel.ITunes), 
  publicNoRedistribute("Public, with no license to redistribute", License.allRightsReserved, PublicationChannel.Matterhorn, PublicationChannel.YouTube, PublicationChannel.ITunes), 
  studentsOnly("Students-only, with no license to redistribute", License.allRightsReserved, PublicationChannel.Matterhorn), ;

  private final String recordingAvailability;
  private final License license;
  private final Set<PublicationChannel> distribution;

  public License getLicense() {
    return license;
  }

  public Set<PublicationChannel> getDistribution() {
    return distribution;
  }
  
  @Override
  public String getSalesforceValue() {
    return recordingAvailability;
  }

  private RecordingAvailability(String recordingAvailability, License license, PublicationChannel... publications) {
    this.recordingAvailability = recordingAvailability;
    this.license = license;
    this.distribution = new HashSet<PublicationChannel>(Arrays.asList(publications));
  }
}
