/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.endpoint;

import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static org.opencastproject.security.api.DefaultOrganization.DEFAULT_ORGANIZATION_ADMIN;
import static org.opencastproject.util.doc.rest.RestParameter.Type.BOOLEAN;
import static org.opencastproject.util.doc.rest.RestParameter.Type.STRING;

import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.CourseUtils;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.security.api.SecurityService;
import org.opencastproject.security.api.User;
import org.opencastproject.util.doc.rest.RestParameter;
import org.opencastproject.util.doc.rest.RestQuery;
import org.opencastproject.util.doc.rest.RestResponse;
import org.opencastproject.util.doc.rest.RestService;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST endpoint for CourseManagement Service.
 * 
 */
@Path("/")
@RestService(name = "coursemanagementservice", title = "CourseManagement Service",
  abstractText = "This service manages Course information", 
  notes = {
        "All paths above are relative to the REST endpoint base (something like http://your.server/files)",
        "If the service is down or not working it will return a status 503, this means the the underlying service is "
        + "not working and is either restarting or has failed",
        "A status code 500 means a general failure has occurred which is not recoverable and was not anticipated. In "
        + "other words, there is a bug! You should file an error report with your server logs from the time when the "
        + "error occurred: <a href=\"https://opencast.jira.com\">Opencast Issue Tracker</a>" })
public class CourseManagementRestService {

  private static final Logger logger = LoggerFactory.getLogger(CourseManagementRestService.class);
  private static final String COURSE_OFFERING_ID = "courseOfferingId";

  /** CourseManagementService **/
  protected CourseManagementService courseManagementService;

  /** SecurityService **/
  protected SecurityService securityService;

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{courseOfferingId:.+}.json")
  @RestQuery(name = "getAsJson", description = "Returns the course with the given identifier", returnDescription = "Returns JSON representation of a CourseOffering", 
              pathParameters = { @RestParameter(name = COURSE_OFFERING_ID, isRequired = true, description = "The CourseOffering identifier, i.e. 2013B39060", type = STRING) }, reponses = {
          @RestResponse(responseCode = SC_OK, description = "The CourseOffering JSON."),
          @RestResponse(responseCode = SC_NOT_FOUND, description = "No CourseOffering with this identifier was found.") })
  public Response getCourseJSON(@PathParam(COURSE_OFFERING_ID) final String courseOfferingId) {
    logger.debug("Course Lookup: {}", courseOfferingId);
    try {
      final Response response;
      final String id = StringUtils.trimToNull(courseOfferingId);
      if (id == null) {
        response = Response.status(Response.Status.NOT_FOUND).build();
      } else {
        final CourseOffering courseOffering = this.courseManagementService.getCourseOffering(id);
        if (courseOffering == null) {
          response = Response.status(Response.Status.NOT_FOUND).build();
        } else if (!CourseUtils.isRoomCapable(courseOffering.getRoom())) {
          response = Response.status(Response.Status.FORBIDDEN).build();
        } else {
          response = Response.ok(courseOffering.toJson()).type(MediaType.APPLICATION_JSON_TYPE).build();          
        }
      }
      return response;
    } catch (Exception e) {
      logger.error("Could not retrieve course: {}", e);
      throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
    }
  }

  @POST
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/approval/{courseOfferingId:.+}")
  @RestQuery(name = "createParticipation", description = "creates and persists a Participation record", returnDescription = "The status of creating a Participation record",
      pathParameters = { @RestParameter(name = COURSE_OFFERING_ID, isRequired = true, description = "The unique identifier of a class of form YEAR SEMESTER_CODE COURSE_CNTL_NUM, e.g. 2013B39060 for History 5 Spring 2013", type = STRING) },
      restParameters = {
          @RestParameter(name = "instructorLdapId", isRequired = true, description = "The instructors CalNet LdapId", type = STRING),
          @RestParameter(name = "recordingType", isRequired = true, description = "The type of capture made, as videoAndScreencast, videoOnly, screencast, audioOnly", type = STRING),
          @RestParameter(name = "approved", isRequired = true, description = "Whether the instructor has approved webcasting the course", type = BOOLEAN),
          @RestParameter(name = "delayPublishByDays", isRequired = true, description = "The number of days to delay publishing", type = STRING),
          @RestParameter(name = "recordingAvailability", isRequired = true, description = "The recording availability selected by the instructor, studentsOnly, publicCreativeCommons, publicNoRedistribute", type = STRING)}, reponses = {
      @RestResponse(responseCode = SC_OK, description = "The final approval status in JSON."),
      @RestResponse(responseCode = SC_NOT_FOUND, description = "No CourseOffering with this identifier was found.") })
  public Response createParticipation(@PathParam(COURSE_OFFERING_ID) final String courseOfferingId,
                                      @FormParam("instructorLdapId") final String userLdapId,
                                      @FormParam("recordingType") final String recordingType,
                                      @FormParam("approved") final Boolean approved,
                                      @FormParam("delayPublishByDays") final String delayPublishByDays,
                                      @FormParam("recordingAvailability") final String recordingAvailability) {
    if (courseOfferingId == null) {
      logger.warn("unique courseOfferingId that should be added is null");
      return Response.status(BAD_REQUEST).build();
    }
    try {
      final boolean isReadyToSchedule;
      final CapturePreferences capPrefs = new CapturePreferences(recordingType.trim(), recordingAvailability.trim(), delayPublishByDays.trim());
      
      logger.info("For user {}, updating CapturePreferences {} with approved {}", new Object[]{userLdapId, capPrefs, approved});
      boolean isAdminUser = !approved && isAdminUser(securityService.getUser());
      logger.info("User {} an admin user {}", new Object[]{userLdapId, isAdminUser});
      final JSONObject approvalComplete = new JSONObject();
      approvalComplete.put(COURSE_OFFERING_ID, courseOfferingId);
      if (!isAdminUser) {
        // approval is only complete and ready to schedule only if all instructors have approved or ...
        isReadyToSchedule = this.courseManagementService.updateCapturePreferences(courseOfferingId, userLdapId, capPrefs);
      } else {
        // approval is complete if an admin user overrides the instructors so not all need approve for scheduling to happen
        isReadyToSchedule = this.courseManagementService.updateCapturePreferencesByAdmin(courseOfferingId, userLdapId, capPrefs);
      }
      approvalComplete.put("isReadyToSchedule", isReadyToSchedule);
      return Response.status(CREATED).entity(approvalComplete.toJSONString()).type(MediaType.APPLICATION_JSON_TYPE).build();
    } catch (Exception e) {
      String msg = "creating participation failed due to " + e.getMessage();
      logger.error(msg, e);
      return Response.status(INTERNAL_SERVER_ERROR).type(MediaType.TEXT_PLAIN).entity(msg).build();
    }
  }

  /*
  * per specification, an admin override approval is sending a false for approved
  * check that and the fact that an admin user must have ROLE_ADMIN
   */
  private boolean isAdminUser(final User user) {
    boolean isAdminUser = false;
    final String[] userRoles = user.getRoles();
    for (String role : userRoles) {
      if (DEFAULT_ORGANIZATION_ADMIN.equals(role)) {
        isAdminUser = true;
        break;
      }
    }
    return isAdminUser;
  }

  /**
   * OSGi callback for setting participation service.
   * @param courseManagementService Null not allowed.
   */
  public void setCourseManagementService(final CourseManagementService courseManagementService) {
    this.courseManagementService = courseManagementService;
  }

  /**
   * OSGi callback for setting security service.
   * @param securityService never null
   */
  public void setSecurityService(final SecurityService securityService) {
    this.securityService = securityService;
  }

  /**
   * Activates REST service.
   *
   * @param cc
   *          ComponentContext
   */
  public void activate(final ComponentContext cc) {
    logger.info("activating CourseManagementService");
  }

}
